// DataProviderDemo.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

typedef void* (*DP_QUERY)(const char*, const char*, int, const char*);
typedef unsigned int (*DP_FETCH_NEXT)(void*, char*, unsigned int, unsigned int);
typedef void (*DP_CLOSE)();
typedef int (*DP_VALIDATE)(const char*, const char*);

void ErrorExit(LPTSTR lpszFunction)
{
    LPVOID lpMsgBuf = NULL;
    LPVOID lpDisplayBuf = NULL;
    DWORD dw = GetLastError();

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR) &lpMsgBuf,
        0,
        NULL);

    lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
                                      (lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));
    StringCchPrintf((LPTSTR)lpDisplayBuf,
                    LocalSize(lpDisplayBuf) / sizeof(TCHAR),
                    _T("%s failed with error %d: %s"),
                    lpszFunction, dw, lpMsgBuf);
    _tprintf(_T("Error: %s\n"), lpDisplayBuf);

    LocalFree(lpMsgBuf);
    LocalFree(lpDisplayBuf);
    ExitProcess(dw);
}

HINSTANCE LoadDll(LPCWSTR lpLibFileName)
{
    HINSTANCE hDll = LoadLibrary(L"DataProvider.dll");
    if (NULL == hDll)
    {
        ErrorExit(_T("LoadLibrary"));
    }
    return hDll;
}

FARPROC GetProc(HINSTANCE hDll, LPCSTR lpProcName)
{
    FARPROC f = NULL;

    if (NULL != hDll)
    {
        f = GetProcAddress(hDll, lpProcName);
        if (NULL == f)
        {
            ErrorExit(_T("GetProcAddress"));
            FreeLibrary(hDll);
        }
    }

    return f;
}

int _tmain(int argc, _TCHAR* argv[])
{
    HINSTANCE hDll = LoadDll(L"DataProvider.dll");
    if (NULL == hDll)
    {
        return -1;
    }

    DP_QUERY dp_query = (DP_QUERY)GetProc(hDll, "dp_query");
    if (NULL == dp_query)
    {
        return -1;
    }

    DP_FETCH_NEXT dp_fetch_next = (DP_FETCH_NEXT)GetProc(hDll, "dp_fetch_next");
    if (NULL == dp_fetch_next)
    {
        return -1;
    }

    DP_CLOSE dp_close = (DP_CLOSE)GetProc(hDll, "dp_close");
    if (NULL == dp_close)
    {
        return -1;
    }

	DP_VALIDATE dp_validate = (DP_VALIDATE)GetProc(hDll, "dp_validate");
	if (NULL == dp_validate)
	{
		return -1;
	}

	void* dp_result_set = dp_query("admin", "admin", 3, "SELECT JH,RQ,SCSJ,RCYL1,RCYL,HS,YY,TY FROM VH_DBA01 ORDER BY JH,RQ");
	if (NULL == dp_result_set)
	{
		_tprintf(_T("dp_query failed.\n"));
		dp_close();
		return -1;
	}

	char data[65535] = {0};
	while (dp_fetch_next(dp_result_set, data, sizeof(data), 10))
	{
		printf("%s\n", data);
	}

    dp_close();

    FreeLibrary(hDll);

    system("pause");

    return 0;
}

