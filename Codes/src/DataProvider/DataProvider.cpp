// DataProvider.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "DataProvider.h"

#define DEFAULT_FETCH_SETP	(1)

typedef enum
{
	DB_1 = 1,	// 股份公司库
	DB_2,		// 油田公司库
	DB_3		// 实际库
};

#define DB_1_INSTANCE	("orcl")
#define DB_1_ACCOUNT	("topp_gf")
#define DB_1_PASSWORD	("topp_gf")

#define DB_2_INSTANCE	("orcl")
#define DB_2_ACCOUNT	("topp_yt")
#define DB_2_PASSWORD	("topp_yt")

#define DB_3_INSTANCE	("orcl")
#define DB_3_ACCOUNT	("topp_sj")
#define DB_3_PASSWORD	("topp_sj")

char* get_db_instance(int db)
{
	switch (db)
	{
	case DB_1:
		return DB_1_INSTANCE;
	case DB_2:
		return DB_2_INSTANCE;
	case DB_3:
	default:
		return DB_3_INSTANCE;
	}
}

char* get_db_account(int db)
{
	switch (db)
	{
	case DB_1:
		return DB_1_ACCOUNT;
	case DB_2:
		return DB_2_ACCOUNT;
	case DB_3:
	default:
		return DB_3_ACCOUNT;
	}
}

char* get_db_password(int db)
{
	switch (db)
	{
	case DB_1:
		return DB_1_PASSWORD;
	case DB_2:
		return DB_2_PASSWORD;
	case DB_3:
	default:
		return DB_3_PASSWORD;
	}
}

int get_local_ip(char ip[][16])
{
	WSADATA wsaData = {0};
	int count = 0;

	if(SOCKET_ERROR == WSAStartup(MAKEWORD(2, 2), &wsaData))
	{
		return count;
	}

	char host_name[256] = {0};
	hostent *host = gethostbyname(host_name);

	// 取得本地主机名称
	gethostname(host_name, sizeof(host_name));

	// 通过主机名得到地址信息
	// 打印出所有IP地址
	in_addr addr = {0};
	while (1)
	{
		char *p = host->h_addr_list[count];
		if(NULL == p)
		{
			break;
		}
		memcpy(&addr.S_un.S_addr, p, host->h_length);
		strcpy_s(ip[count], sizeof(ip[count]), inet_ntoa(addr));	// 将32位的二进制数转化为字符串
		count++;
	}

	WSACleanup();

	return count;
}

void md5_hex(char *enc, char *dec)
{
	MD5_CTX md5;
	MD5Init(&md5);
	unsigned char *encrypt = (unsigned char*)enc;
	unsigned char decrypt[16];
	char tmp[3] = {0};
	MD5Update(&md5, encrypt, strlen((char *)encrypt));
	MD5Final(&md5, decrypt);
	for(int i = 0; i < 16; i++)
	{
		memset(tmp, 0, sizeof(tmp));
		sprintf_s(tmp, sizeof(tmp), "%02X",decrypt[i]);
		strcat_s(dec, 33, tmp);
	}
}

DATAPROVIDER_API void* dp_query(const char* username, const char* password, int db, const char *sql)
{
	if (-1 == dp_validate(username, password))
	{
		return NULL;
	}

    OCI_Statement *st = NULL;
    OCI_Connection *cn = NULL;
    OCI_Resultset *rs = NULL;

    if (OCI_Initialize(NULL, NULL, OCI_ENV_DEFAULT))
    {
        if (NULL != (cn = OCI_ConnectionCreate(get_db_instance(db), get_db_account(db), get_db_password(db), OCI_SESSION_DEFAULT)))
        {
            if ((NULL != (st = OCI_StatementCreate(cn))) && OCI_ExecuteStmt(st, sql))
            {
                rs = OCI_GetResultset(st);
            }
        }
    }

    return rs;
}

DATAPROVIDER_API unsigned int dp_fetch_next(void *result_set, char *record, unsigned int max_len, unsigned int step)
{
    OCI_Resultset *rs = (OCI_Resultset*)result_set;
    unsigned int sp = step > 0 ? step : DEFAULT_FETCH_SETP;
	unsigned int row = 0;
    char* tmp = (char*)malloc(max_len);

    memset(record, 0, max_len);
    memset(tmp, 0, max_len);
    record[0] = '{';

    if (NULL != rs)
    {
        unsigned int column_count = OCI_GetColumnCount(rs);

        strcat_s(record, max_len, "\"rows\":[");

        for (; row < sp && OCI_FetchNext(rs); ++row)
        {
            strcat_s(record, max_len, "{");

            for (int col = 1; col <= column_count; ++col)
            {
                memset(tmp, 0, max_len);

                OCI_Column *column = OCI_GetColumn(rs, col);
                if (NULL != column)
                {
                    const char* column_name = OCI_ColumnGetName(column);
					const char* column_sql_type = OCI_ColumnGetSQLType(column);

                    if (0 == strcmp(column_sql_type, "NVARCHAR2") || 0 == strcmp(column_sql_type, "VARCHAR2") || 0 == strcmp(column_sql_type, "CHAR"))
                    {
						sprintf_s(tmp, max_len, "\"%s\":\"%s\"", column_name, OCI_GetString(rs, col));
                    }
					else if (0 == strcmp(column_sql_type, "NUMBER"))
					{
						sprintf_s(tmp, max_len, "\"%s\":\"%f\"", column_name, OCI_GetDouble(rs, col));
					}
					else if (0 == strcmp(column_sql_type, "DATE"))
					{
						char dt[9] = {0};
						OCI_DateToText(OCI_GetDate(rs, col), "YYYYMMDD", sizeof(dt), dt);
						sprintf_s(tmp, max_len, "\"%s\":\"%s\"", column_name, dt);
					}
                }

                if (col < column_count)
                {
                    strcat_s(tmp, max_len, ",");
                }

                strcat_s(record, max_len, tmp);
            }

            strcat_s(record, max_len, "},");
        }

		memset(tmp, 0, max_len);
		sprintf_s(tmp, max_len, "],\"total\":%d", row);

		unsigned int record_len = strlen(record);
		record[record_len - 1] = '\0';
		strcat_s(record, max_len, tmp);
    }

    strcat_s(record, max_len, "}");

	free(tmp);
	tmp = NULL;

	return row;
}

DATAPROVIDER_API void dp_close()
{
    OCI_Cleanup();
}

DATAPROVIDER_API int dp_validate(const char* username, const char* password)
{
	OCI_Statement *st = NULL;
	OCI_Connection *cn = NULL;
	OCI_Resultset *rs = NULL;
	char sql[1024] = {0};
	char value[256] = {0};
	char ip[256][16];
	char ip_str[8192] = {0};
	int ret = -1;

	char pwd[33] = {0};
	md5_hex((char*)password, pwd);

	memset(ip, 0, 256 * 16);
	int n = get_local_ip(ip);

	for (int i = 0; i < n; ++i)
	{
		strcat_s(ip_str, sizeof(ip_str), ip[i]);
		strcat_s(ip_str, sizeof(ip_str), ",");
	}
	unsigned len = strlen(ip_str);
	ip_str[len - 1] = '\0';

	sprintf_s(sql, sizeof(sql), "select 1 from TBUSER where USERID=\'%s\' and USERPWD=\'%s\' and CHECKSIP(\'%s\',IP)>0 and rownum=1", username, pwd, ip_str);

	if (OCI_Initialize(NULL, NULL, OCI_ENV_DEFAULT))
	{
		if (NULL != (cn = OCI_ConnectionCreate(DB_3_INSTANCE, DB_3_ACCOUNT, DB_3_PASSWORD, OCI_SESSION_DEFAULT)))
		{
			if ((NULL != (st = OCI_StatementCreate(cn))) && OCI_ExecuteStmt(st, sql))
			{
				if (NULL != (rs = OCI_GetResultset(st)))
				{
					while (OCI_FetchNext(rs))
					{
						if (1 == OCI_GetInt(rs, 1))
						{
							ret = 0;
							break;
						}
					}
				}
			}
		}
	}
	
	OCI_Cleanup();
	
	return ret;
}
