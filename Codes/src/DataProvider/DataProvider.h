#ifndef __DATAPROVIDER_H__
#define __DATAPROVIDER_H__

#include "ocilib.h"

#ifdef DATAPROVIDER_EXPORTS
#define DATAPROVIDER_API __declspec(dllexport)
#else
#define DATAPROVIDER_API __declspec(dllimport)
#endif

#ifdef __cplusplus
#if __cplusplus
extern "C"
{
#endif
#endif

/**
 * @brief      查询数据库
 *
 * @param[in]  username    用户名
 * @param[in]  password    密码
 * @param[in]  db    数据库选择(1: 股份公司库, 2: 油田公司库, 3: 实际库)
 * @param[in]  sql   SQL语句
 *
 * @return     结果集
 */
DATAPROVIDER_API void* dp_query(const char* username, const char* password, int db, const char *sql);

/**
 * @brief      获取记录
 *
 * @param      result_set  结果集
 * @param      data        返回记录缓冲区(JSON格式)
 * @param[in]  len         返回记录缓冲区最大字节数
 * @param[in]  step        最大记录行数
 *
 * @return     { 记录数 }
 */
DATAPROVIDER_API unsigned int dp_fetch_next(void *result_set, char *record, unsigned int max_len, unsigned int step);

/**
 * @brief      释放结果集
 */
DATAPROVIDER_API void dp_close();

/**
 * @brief      校验用户
 *
 * @param[in]  username    用户名
 * @param[in]  password    密码
 *
 * @return     { 0: 校验通过, -1: 校验失败 }
 */
DATAPROVIDER_API int dp_validate(const char* username, const char* password);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif

#endif	/* __DATAPROVIDER_H__ */
