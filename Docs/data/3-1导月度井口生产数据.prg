set talk off
set exact on

yyyy=year(datetime())
mm=month(datetime())-1

	do case
		case mm=0
			yymm1=str(yyyy-1,4,0)+str(12,2,0)
			yymm2=str(yyyy-1,4,0)+str(12,2,0)
		case mm<10
			yymm1=str(yyyy,4,0)+str(0,1,0)+str(mm,1,0)
			yymm2=str(yyyy,4,0)+str(mm,1,0)
		case mm>=10
			yymm1=str(yyyy,4,0)+str(mm,2,0)
			yymm2=str(yyyy,4,0)+str(mm,2,0)
	endcase
	
	dufile='h:\red\dbf\du'+yymm1+'.dbf'
	dsfile='h:\red\dbf\ds'+yymm1+'.dbf'
	
	dufile1='h:\三厂地质月报du'+yymm2+'.dbf'
	
	
	dsfile1='h:\三厂地质月报ds+yymm2+'.dbf'
	


	
use h:\du.dbf
zap
appe from &dufile1


use h:\ds.dbf
zap
appe from &dsfile1

close data

sele 1
use h:\du
pack
sele 2
use &dufile
sele 1
go top
do while .not. eof()
   sele 2
   loca for alltrim(well)==alltrim(a->well)
   if found()
      *repl tcr with a->tcrq
      repl cw with a->cw,u1 with a->u1,u5 with a->u5,u6 with a->u6
      repl u4 with a->u4,u7 with a->u7,u8 with a->u8
      repl u12 with round(a->u12,0),u13 with round(a->u13,0),u14 with a->u14,u23 with a->u23
      repl u22 with a->u22,u21 with a->u21,u29 with a->u29,bz with trim(a->bz)
      repl u14 with a->u14
     
      repl u3 with a->u3
   else
      ?trim(a->well)+'井在月报库中不存在,可能为新井!'  
 endif
   sele 1
   skip
enddo

sele 1
use h:\ds
pack
sele 2
use &dsfile
sele 1
go top
do while .not. eof()
   sele 2
   loca for trim(well)=trim(a->well)
   if found()
      repl cw with a->cw,s1 with a->s1,s6 with a->s6
      repl s3 with a->s3,s4 with a->s4,s2 with a->s2,s11 with a->s11
      repl s8 with a->s8,bz with trim(a->bz)
   else
      ?trim(a->well)+'井在月报库中不存在,可能为新投注井,转注井!'
   endif
   sele 1
   skip
enddo
sele 1
use
sele 2
use
?'over'
close data