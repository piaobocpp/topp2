*****************************
***程序名:CE1.PRG
***功  能:建立各数据库文件
***日  期:1994.9.22.
******接收参数**********
  PARA qi,yi
************************
* DO CERQ
  SET TALK OFF
  SET SAFETY OFF
  SET EXACT ON
  SELE 10
  GO RE
  YT=re
  lq=0
  LM=0
  cq='0'
  STORE '..\dbf\'+'DS'+YY+MM+'.DBF' TO FIL1
  STORE '..\dbf\'+'DU'+YY+MM+'.DBF' TO FIL
  STORE '..\dbf\'+'KF'+YY+MM+'.DBF' TO FIL2
  STORE '..\dbf\'+YY+MM+'-A'+'.DBF' TO FIL3
  STORE '..\dbf\'+YY+MM+'-B'+'.DBF' TO FIL4
  STORE '..\dbf\'+YY+MM+'-C'+'.DBF' TO FIL5 
  STORE '..\DBF\'+'UC'+YY+MM+'.DBF' TO FIL6
  STORE '..\DBF\'+'SC'+YY+MM+'.DBF' TO FIL7
  STORE 0 TO Y1,Y2,Y3,Y4,Y5,Y6
  DO WHILE YT<RD-1
     IF LEN(TRIM(CONT))=12
        STORE '..\dbf\'+TRIM(COLOR1)+YY+MM+TRIM(COLOR2)+'.DBF' TO RFIL
        STORE '..\dbf\'+TRIM(COLOR1)+YY3+MM3+TRIM(COLOR2)+'.DBF' TO EFIL
        IF .NOT.FILE("&EFIL")
           save screen
           do openwin with 10,7,12,74,"3","+1",'文件'+EFIL+'不存在(初次使用请进初始化)按任意键返回!'
           WAIT""
           restore screen
           RETURN
        ENDIF
        IF LQ=16
           LQ=0
           LM=26
           CQ='5'
        ENDIF
        SET COLO TO W+/&cq+
        @3+lq,56-LM SAY '┎─────────┒'
        @4+lq,56-LM SAY '┃                  ┃'
        @5+lq,56-LM SAY '┠─────────┨'
        @6+lq,56-LM SAY '┃                  ┃'
        @7+lq,56-LM SAY '┖─────────┚'
        @4+lq,61-LM SAY SUBSTR(CONT,1,10)
        @6+lq,58-LM SAY '正在建该数据库...'
        SELE 1
        USE &EFIL
        DO WHILE .T.
           IF FILE("&RFIL")
              SET COLO TO W/R+
              save screen
              do openwin with 10,15,12,65,"3","+1",'该数据库已存在是否重建(y/n)'
              xq=inkey(0)
              restore screen  
              DO CASE
                 CASE xq=89.OR.XQ=121
                   SET CONsole off
                   SET HELP OFF
                   COPY TO &RFIL
                   SET COLO TO W/W+
                 * @12,25 CLEAR TO 12,60
                   USE
                   IF '&RFIL'='&FIL'
                      Y1=1
                   ENDIF
                   IF '&RFIL'='&FIL1'
                      Y2=1
                   ENDIF
                   IF '&RFIL'='&FIL2'
                      Y3=1
                   ENDIF
                   IF '&RFIL'='&FIL3'
                      Y4=1
                   ENDIF
                   IF '&RFIL'='&FIL4'
                      Y5=1
                   endif
                   IF '&RFIL'='&FIL5'               
                      Y6=1
                   ENDIF
                   EXIT
                 case xq=78.OR.XQ=110
                   SET COLO TO W/W+
                 * @12,25 CLEAR TO 12,60
                   USE
                   EXIT
              ENDCASE
            ELSE
              COPY TO &RFIL
              USE
              IF '&RFIL'='&FIL'
                 Y1=1
              ENDIF
              IF '&RFIL'='&FIL1'
                 Y2=1
              ENDIF
              IF '&RFIL'='&FIL2'
                 Y3=1
              ENDIF
              IF '&RFIL'='&FIL3' 
                 Y4=1
              ENDIF
              IF '&RFIL'='&FIL4'
                 Y5=1
              ENDIF
              IF '&RFIL'='&FIL5'
                 Y6=1
              ENDIF
              EXIT
           ENDIF
        ENDDO
        lq=lq+2
        LM=LM+2
        yt=yt+1
        Rq=val(cq)+1
        cq=str(rq,1,0)
        SELE 10
        SKIP
        LOOP
      ELSE
        sele 10
        skip
        YT=YT+1
        loop
     ENDIF
  ENDDO
  IF Y1=1
     IF FILE("&FIL")
        SELE 1
        USE ..\PRG\USWCL
        APPE FROM &FIL FOR UPPER(B5)='A'.OR.UPPER(B5)='B'
        USE
        USE &FIL
        REPL ALL U1 WITH 0,U13 WITH 0,U14 WITH 0
        REPL all U17 WITH 0,U21 WITH '',U22 WITH 0,U23 WITH 0
        REPL ALL U9 WITH 0,U10 WITH 0,U11 WITH 0,U24 WITH 0
        REPL ALL U25 WITH 0,U12 WITH 0,U16 WITH 0,U29 WITH 0
        DELE ALL FOR UPPER(B5)='A'.OR.UPPER(B5)='B'
        PACK
        USE
     ENDIF
  ENDIF
  IF Y2=1
     IF FILE("&FIL1")
        SELE 1
        USE ..\PRG\SSWCL
        APPE FROM &FIL1 FOR UPPER(B5)='A'.OR.UPPER(B5)='D'
        USE
        USE &FIL1
        REPL ALL S1 WITH 0,S6 WITH 0,S7 WITH 0,S8 WITH 0
        REPL all S9 WITH 0,S11 WITH 0,S5 WITH 0,S12 WITH 0
        REPL ALL B10 WITH ''
        DELE ALL FOR UPPER(B5)='A'.OR.UPPER(B5)='D'
        PACK
        USE
     ENDIF
  ENDIF
  IF Y3=1
     IF FILE("&FIL2")
        SELE 1 
        USE &FIL2
        ZAP 
        APPE FROM OF-P 
        USE
     ENDIF
  ENDIF
  IF Y4=1
     IF FILE("&FIL3")
        SELE 1    
        USE &FIL3
        ZAP
        APPE FROM OF-P  
        USE
     ENDIF
  ENDIF  
  IF Y5=1
     IF FILE("&FIL4")
        SELE 1
        USE &FIL4
        ZAP
        APPE FROM OF-P 
        USE
     ENDIF
  ENDIF
  IF Y6=1
     IF FILE("&FIL5")
        SELE 1
        USE &FIL5
        ZAP
        APPE FROM OF-P
        USE
     ENDIF
  ENDIF
  IF MM='01'
     SELE 1
     USE &FIL6
     ZAP
     USE
     USE &FIL7
     ZAP
     USE
  ENDIF
  save screen
  do openwin with 10,20,12,60,"1","+1", '完成按任意键返回!'
  WAIT""
  restore screen
  RETURN
