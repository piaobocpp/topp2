********************
*程序名:CEYN.PRG
*功  能:计算年天数,月天数
*日  期:1995.1.3.
*编  程:张吉来
*********************
IF MOD(VAL(YY),4)=0
   YDN=366
 ELSE
   YDN=365
ENDIF
IF MM='02'.AND.MOD(VAL(YY),4)=0
   MDY=29
 ELSE
   MDY=28
ENDIF
DO CASE
   CASE MM='04'.OR.MM='06'.OR.MM='09'.OR.MM='11'
        MDY=30
   CASE MM='01'.OR.MM='03'.OR.MM='05'.OR.MM='07'.OR.MM='08'.OR.MM='10'.OR.MM='12'
        MDY=31
ENDCASE
NN='&MM'
NNY=0
IF NN='01'
   NNY=MDY
   RETURN
  ELSE
   IF NN='12'
      NNY=YDN
      RETURN
   ENDIF
ENDIF
DO WHILE VAL(NN)>=1
   NN=STR(&NN,2)
   IF NN=' 2'.AND.MOD(VAL(YY),4)=0
      NY=29
    ELSE
      NY=28
   ENDIF
   DO CASE
      CASE NN=' 4'.OR.NN=' 6'.OR.NN=' 9'.OR.NN='11'
           NY=30
      CASE NN=' 1'.OR.NN=' 3'.OR.NN=' 5'.OR.NN=' 7'.OR.NN=' 8'.OR.NN='10'.OR.NN='12'
           NY=31
   ENDCASE
   NNY=NNY+NY
   NN=STR(&NN-1,2)
ENDDO
RETURN
