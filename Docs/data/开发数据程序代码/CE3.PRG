********************
*程序名:CE3.PRG
*功  能:计算数据
*日  期:1995.1.3.
*编  程:张吉来
*********************
PARA W,T
******************
SET TALK OFF
SET EXACT ON
CLOSE DATABASES
do ce2
STORE '..\DBF\'+'DU'+YY+MM+'.DBF' TO FN
STORE '..\DBF\'+'DU'+YY3+MM3+'.DBF' TO FNA
IF .NOT.FILE("&FN").or..not.file("&fna")
   do openwin with 10,10,12,76,"1","+1", "本月数据文件"+substr("&FN",8,10)+"不存在按任意键返回!"
   WAIT""
   RETURN
ENDIF
SELE 3
USE OF-P
SELE 2
USE &FNA
GO TOP
SELE 1
USE &FN
IF B10=' '
   do openwin with 10,10,12,70,"2","+1",'没有输入当月数据请输入后再计算按任意键返回!'
   WAIT""
   RETURN
ENDIF
GO BOTTOM
IF B10=' '
   do openwin with 10,10,12,70,"3","+1",'当月数据输入没有结束请输入结束后再计算按任意键返回!'
   WAIT""
   RETURN
ENDIF
SET COLO TO /N
@9,21 CLEAR TO 16,61
SET COLO TO  W/RG
@8,20 CLEAR TO 15,60
@11,26 SAY '正在计算油井月数据....'
S1='—'
S2='／'
S3='＼'
X='1'
S=' '
SELE 1
GO TOP
DO WHILE .NOT.EOF()
   SET COLO TO G+/RG
   STORE WELL TO NAME
   STORE OFN TO QKNA
   @11,49 SAY S&X
   X=STR(&X+1,1)
   IF X='4'
      X='1'
   ENDIF
   IF MM='01'
      REPL U26 WITH 0
      REPL U27 WITH 0
      REPL U28 WITH 0
      REPL U31 WITH 0
      REPL U32 WITH 0
      REPL B6 WITH ''
   ENDIF
   IF U1=0
      REPL U7 WITH 0,U8 WITH ''
      SKIP
      LOOP
   ENDIF
   SELE 3
   LOCATE FOR OFN='&QKNA'
   SELE 2
   LOCATE FOR TRIM(WELL)=TRIM('&NAME')
   IF EOF()
      SET COLO TO R/B
      @23,10 SAY TRIM('&NAME')+'井在上月数据文件中不存在是否为新井(Y/N)'
      set colo to 1/3,1/3
      T=TRIM('&NAME')
      do while .t.
         @23,49+LEN(T) GET S PICTURE "A"
         READ
         IF S='N'.OR.S='n'
            SET COLO TO /W+
            @3,2 CLEAR TO 21,77
            RETURN
         ENDIF
         IF S='Y'.OR.S='y'
            SET COLO TO 3/n
            @23,2 SAY '                                                            '
            EXIT
         ENDIF
      ENDDO
   ENDIF
   SELE 1
   if u1#0
      REPL U10 WITH ROUND(U12/U1,2)
      REPL U11 WITH ROUND(U13/U1,2)
      if U12#0
         REPL U16 WITH ROUND(U14/U12,2)
      endif 
      if MM="01"
         repl U26 with U12
         repl U27 with round(U14/10000,4)
         repl U28 with U13
        else
         REPL U26 WITH (B->U26+U12)
         REPL U27 WITH ROUND((B->U27+(U14/10000)),4)
         REPL U28 WITH (B->U28+U13)
      endif
      REPL U18 WITH (B->U18+U12)
      REPL U19 WITH (B->U19+U13)
      REPL U20 WITH ROUND((B->U20+(U14/10000)),4)
      IF C->D5#0
         REPL U9 WITH ROUND(U10/C->D5+U11,2)
         if u9#0
            REPL U24 WITH ROUND(U11/U9*100,1)
         endif
      ENDIF
      IF U21#'未测'.OR.U21#'下封'
         REPL U25 WITH ROUND(U3-VAL(U21),0)
        else
         repl u25 with 0
      ENDIF
   endif
   SELE 1
   SKIP
ENDDO
SET COLO TO /W+
@3,2 CLEAR TO 21,77
RETURN
