*******计算股份公司201412综合开发数据*******
set safety off

?'计算股份公司201412综合开发数据'

use D:\月综合开发数据\201412\股份公司201412\ds201412
copy to D:\月综合开发数据\201412\股份公司201412\股份公司ds201412.xls type xl5
use D:\月综合开发数据\201412\股份公司201412\du201412
copy to D:\月综合开发数据\201412\股份公司201412\股份公司du201412.xls type xl5
use D:\月综合开发数据\201412\股份公司201412\uc201412
copy to D:\月综合开发数据\201412\股份公司201412\股份公司uc201412.xls type xl5
copy to D:\月综合开发数据\201412\股份公司201412\uc.xls type xl5
use D:\月综合开发数据\201412\股份公司201412\sc201412
copy to D:\月综合开发数据\201412\股份公司201412\股份公司sc201412.xls type xl5
copy to D:\月综合开发数据\201412\股份公司201412\sc.xls type xl5

USE d:\月综合开发数据\201412\股份公司201412\Du201412.dbf EXCLUSIVE
COPY TO d:\月综合开发数据\201412\股份公司201412\采油单井.XLS FIELDS Du201412.well,du201412.ofn,Du201412.u1,Du201412.u2,Du201412.u15,Du201412.u7,Du201412.u8,Du201412.u3,Du201412.u4,Du201412.u5,Du201412.u6,Du201412.u9,Du201412.u10,Du201412.u11,Du201412.u12,Du201412.u13,Du201412.u14,Du201412.u17,Du201412.u18,Du201412.u19,Du201412.u20,Du201412.u22,Du201412.u21,Du201412.u24,Du201412.u23,Du201412.u29,Du201412.u25,Du201412.u16,Du201412.BZ TYPE XL5
USE d:\月综合开发数据\201412\股份公司201412\Ds201412.dbf EXCLUSIVE
COPY TO d:\月综合开发数据\201412\股份公司201412\注水单井.XLS FIELDS Ds201412.well,ds201412.ofn,Ds201412.cw,Ds201412.s1,Ds201412.s15,Ds201412.s2,Ds201412.s3,Ds201412.s4,Ds201412.s9,Ds201412.s13,Ds201412.s5,Ds201412.s6,Ds201412.s14,Ds201412.s10,Ds201412.s11,Ds201412.s7,Ds201412.s8,Ds201412.s12,Ds201412.bz TYPE XL5
USE d:\月综合开发数据\201412\股份公司201412\Kf201412.dbf EXCLUSIVE
COPY TO d:\月综合开发数据\201412\股份公司201412\开发数据1.xls fields ofn,k1,k2,k8,k5,k6,k49,k24,k25,k26,k52,k53,k54,k4,k15,k50 type xl5
COPY TO d:\月综合开发数据\201412\股份公司201412\开发数据2.XLS FIELDS ofn,Kf201412.k8,Kf201412.k10,Kf201412.k16,Kf201412.k17,Kf201412.k9,Kf201412.k11,Kf201412.k22,Kf201412.k23,Kf201412.k7,Kf201412.k42,Kf201412.k43,Kf201412.k44 TYPE XL5
COPY TO d:\月综合开发数据\201412\股份公司201412\开发数据3.XLS FIELDS Kf201412.ofn,Kf201412.k13,Kf201412.k14,Kf201412.k20,Kf201412.k21,Kf201412.k56,Kf201412.k57,Kf201412.k58,Kf201412.k59,Kf201412.k45,Kf201412.k46,Kf201412.k60,Kf201412.k47 TYPE XL5
COPY TO d:\月综合开发数据\201412\股份公司201412\开发数据4.XLS FIELDS Kf201412.ofn,Kf201412.k12,Kf201412.k18,Kf201412.k19,Kf201412.k31,Kf201412.k32,Kf201412.k33,Kf201412.k34,Kf201412.k35,Kf201412.k36,Kf201412.k37,Kf201412.k38,Kf201412.k39,Kf201412.k40 TYPE XL5
sele 1
USE d:\月综合开发数据\201412\股份公司201412\201412-a
sele 2
USE d:\月综合开发数据\201412\股份公司201412\201412-c
sele 1
COPY TO d:\月综合开发数据\201412\股份公司201412\产油量构成1.dbf fields ofn,a5,a47,a49,b->c1,b->c2,b->c3,b->c4,a72,a73,a74,a77,a79,a78,a80,a81,a84,a85 
  sele 4
   USE d:\月综合开发数据\201412\股份公司201412\产油量构成1.dbf
    go top
    do while not eof()
     sele 2
      loca for ofn=d->ofn
       if found()
         sele 4
         repl c1 with b->c1,c2 with b->c2,c3 with b->c3,c4 with b->c4
       endif
   sele 4
   skip
   enddo
sele 4
 copy to  d:\月综合开发数据\201412\股份公司201412\产油量构成1.XLS  type XL5
 sele 3  
USE d:\月综合开发数据\201412\股份公司201412\201412-b
COPY TO d:\月综合开发数据\201412\股份公司201412\产油量构成2.xls fields ofn,b1,b3,b4,b5,b8,b10,b11,b12,b15,b17,b18,b19,b22,b24,b25,b26 type xl5
COPY TO d:\月综合开发数据\201412\股份公司201412\产油量构成3.xls fields ofn,b29,b31,b32,b33,b36,b38,b39,b40,b43,b45,b46,b47,b50,b52,b53,b54,b57,b59,b60,b61 type xl5
COPY TO d:\月综合开发数据\201412\股份公司201412\产油量构成4.xls fields ofn,b64,b66,b67,b68,b71,b73,b74,b75,b78,b80,b81,b82,b87,b89,b90,b91,b94,b96,b97,b98 type xl5
sele 2
COPY TO d:\月综合开发数据\201412\股份公司201412\新老区新井产液量表.xls fields ofn,c1,c2,c5,c3,c4,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21 type xl5
sele 1
USE d:\月综合开发数据\201412\股份公司201412\201412-a
COPY TO d:\月综合开发数据\201412\股份公司201412\油田注水量构成数据表1.dbf fields ofn,a12,a63,a66,b->c25,b->c23,b->c24,b->c26,b->c27,b->c28,b->c29,b->c30,b->c31,b->c32,b->c33,b->c34,b->c35,b->c36 
  sele 4
   USE d:\月综合开发数据\201412\股份公司201412\油田注水量构成数据表1.dbf
   go top
   do while not eof()
    sele 2
    loca for ofn=d->ofn
    if found()
     sele 4
     repl c25 with b->c25,c23 with b->c23,c24 with b->c24,c26 with b->c26,c27 with b->c27,c28 with b->c28,c29 with b->c29,c30 with b->c30
     repl c31 with b->c31,c32 with b->c32,c33 with b->c33,c34 with b->c34,c35 with b->c35,c36 with b->c36
     endif
    sele 4
    skip
    enddo
  sele 4
  COPY TO d:\月综合开发数据\201412\股份公司201412\油田注水量构成数据表1.xls type xl5
  
sele 2
COPY TO d:\月综合开发数据\201412\股份公司201412\油田注水量构成数据表2.xls fields ofn,c37,c38,c39,c40,c41,c42,c43,c44,c45,c46,c47,c48,c49,c50,c51,c52 type xl5
sele 1
COPY TO d:\月综合开发数据\201412\股份公司201412\油水井动态表.xls fields ofn,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18 type xl5
sele 1
COPY TO d:\月综合开发数据\201412\股份公司201412\产液量构成表1.dbf fields ofn,a55,a57,b->c13,b->c14,a75,a76,a82,a83,c->b6,c->b7,c->b13,c->b14
   sele 4
    USE d:\月综合开发数据\201412\股份公司201412\产液量构成表1.dbf
    go top
     do while not eof()
      sele 2
      loca for ofn=d->ofn
       if found()
        sele 4
        repl c13 with b->c13,c14 with b->c14
       endif
     sele 3
     loca for ofn=d->ofn
       if found()
        sele 4
        repl b6 with c->b6,b7 with c->b7,b13 with c->b13,b14 with c->b14
       endif
    sele 4
    skip
    enddo
  COPY TO d:\月综合开发数据\201412\股份公司201412\产液量构成表1.xls type xl5
sele 3
COPY TO d:\月综合开发数据\201412\股份公司201412\产液量构成表2.xls fields  ofn,b20,b21,b27,b28,b34,b35,b41,b42,b48,b49,b55,b56 type xl5
COPY TO d:\月综合开发数据\201412\股份公司201412\产液量构成表3.dbf fields  ofn,b62,b63,b69,b70,b92,b93,b99,b100,b->c25,b->c53,b->c54,b->c55,b->c56 
 sele 4
  USE d:\月综合开发数据\201412\股份公司201412\产液量构成表3
  go top
   do while not eof()
     sele 2
     loca for ofn=d->ofn
      if found()
       sele 4
       repl c25 with b->c25,c53 with b->c53,c54 with b->c54,c55 with b->c55,c56 with b->c56
      endif
   sele 4
   skip
  enddo
  COPY TO d:\月综合开发数据\201412\股份公司201412\产液量构成表3.xls type xl5
USE d:\月综合开发数据\201412\股份公司201412\kf201412
COPY TO d:\月综合开发数据\201412\股份公司201412\油田（区块）开发综合数据表1.xls fields ofn,k1,k2,k3,k4,k5,k6,k8,k10,k16,k17,k9,k11,k22,k23,k7,k42,k43,k44,k12,k18,k19,k13,k14,k20,k21 type xl5
COPY TO d:\月综合开发数据\201412\股份公司201412\油田（区块）开发综合数据表2.xls fields ofn,k24,k25,k26,k52,k53,k54,k27,k28,k55,k15,k29,k30,k50,k41,k31,k32,k33,k34,k35,k36,k37,k38,k39,k40 type xl5
close data
?'股份公司201412综合开发数据计算完毕'

*******计算实际201412综合开发数据*******
?'计算实际201412综合开发数据'

use D:\月综合开发数据\201412\实际201412\ds201412
copy to D:\月综合开发数据\201412\实际201412\实际ds201412.xls type xl5
use D:\月综合开发数据\201412\实际201412\du201412
copy to D:\月综合开发数据\201412\实际201412\实际du201412.xls type xl5
use D:\月综合开发数据\201412\实际201412\uc201412
copy to D:\月综合开发数据\201412\实际201412\实际uc201412.xls type xl5
copy to D:\月综合开发数据\201412\实际201412\uc.xls type xl5
use D:\月综合开发数据\201412\实际201412\sc201412
copy to D:\月综合开发数据\201412\实际201412\实际sc201412.xls type xl5
copy to D:\月综合开发数据\201412\实际201412\sc.xls type xl5


USE d:\月综合开发数据\201412\实际201412\Du201412.dbf EXCLUSIVE
COPY TO d:\月综合开发数据\201412\实际201412\采油单井.XLS FIELDS Du201412.well,du201412.ofn,Du201412.u1,Du201412.u2,Du201412.u15,Du201412.u7,Du201412.u8,Du201412.u3,Du201412.u4,Du201412.u5,Du201412.u6,Du201412.u9,Du201412.u10,Du201412.u11,Du201412.u12,Du201412.u13,Du201412.u14,Du201412.u17,Du201412.u18,Du201412.u19,Du201412.u20,Du201412.u22,Du201412.u21,Du201412.u24,Du201412.u23,Du201412.u29,Du201412.u25,Du201412.u16,Du201412.BZ TYPE XL5
USE d:\月综合开发数据\201412\实际201412\Ds201412.dbf EXCLUSIVE
COPY TO d:\月综合开发数据\201412\实际201412\注水单井.XLS FIELDS Ds201412.well,ds201412.ofn,Ds201412.cw,Ds201412.s1,Ds201412.s15,Ds201412.s2,Ds201412.s3,Ds201412.s4,Ds201412.s9,Ds201412.s13,Ds201412.s5,Ds201412.s6,Ds201412.s14,Ds201412.s10,Ds201412.s11,Ds201412.s7,Ds201412.s8,Ds201412.s12,Ds201412.bz TYPE XL5
USE d:\月综合开发数据\201412\实际201412\Kf201412.dbf EXCLUSIVE
COPY TO d:\月综合开发数据\201412\实际201412\开发数据1.xls fields ofn,k1,k2,k8,k5,k6,k49,k24,k25,k26,k52,k53,k54,k4,k15,k50 type xl5
COPY TO d:\月综合开发数据\201412\实际201412\开发数据2.XLS FIELDS ofn,Kf201412.k8,Kf201412.k10,Kf201412.k16,Kf201412.k17,Kf201412.k9,Kf201412.k11,Kf201412.k22,Kf201412.k23,Kf201412.k7,Kf201412.k42,Kf201412.k43,Kf201412.k44 TYPE XL5
COPY TO d:\月综合开发数据\201412\实际201412\开发数据3.XLS FIELDS Kf201412.ofn,Kf201412.k13,Kf201412.k14,Kf201412.k20,Kf201412.k21,Kf201412.k56,Kf201412.k57,Kf201412.k58,Kf201412.k59,Kf201412.k45,Kf201412.k46,Kf201412.k60,Kf201412.k47 TYPE XL5
COPY TO d:\月综合开发数据\201412\实际201412\开发数据4.XLS FIELDS Kf201412.ofn,Kf201412.k12,Kf201412.k18,Kf201412.k19,Kf201412.k31,Kf201412.k32,Kf201412.k33,Kf201412.k34,Kf201412.k35,Kf201412.k36,Kf201412.k37,Kf201412.k38,Kf201412.k39,Kf201412.k40 TYPE XL5
sele 1
USE d:\月综合开发数据\201412\实际201412\201412-a
sele 2
USE d:\月综合开发数据\201412\实际201412\201412-c
sele 1
COPY TO d:\月综合开发数据\201412\实际201412\产油量构成1.dbf fields ofn,a5,a47,a49,b->c1,b->c2,b->c3,b->c4,a72,a73,a74,a77,a79,a78,a80,a81,a84,a85 
  sele 4
   USE d:\月综合开发数据\201412\实际201412\产油量构成1.dbf
    go top
    do while not eof()
     sele 2
      loca for ofn=d->ofn
       if found()
         sele 4
         repl c1 with b->c1,c2 with b->c2,c3 with b->c3,c4 with b->c4
       endif
   sele 4
   skip
   enddo
sele 4
 copy to  d:\月综合开发数据\201412\实际201412\产油量构成1.XLS  type XL5
 sele 3  
USE d:\月综合开发数据\201412\实际201412\201412-b
COPY TO d:\月综合开发数据\201412\实际201412\产油量构成2.xls fields ofn,b1,b3,b4,b5,b8,b10,b11,b12,b15,b17,b18,b19,b22,b24,b25,b26 type xl5
COPY TO d:\月综合开发数据\201412\实际201412\产油量构成3.xls fields ofn,b29,b31,b32,b33,b36,b38,b39,b40,b43,b45,b46,b47,b50,b52,b53,b54,b57,b59,b60,b61 type xl5
COPY TO d:\月综合开发数据\201412\实际201412\产油量构成4.xls fields ofn,b64,b66,b67,b68,b71,b73,b74,b75,b78,b80,b81,b82,b87,b89,b90,b91,b94,b96,b97,b98 type xl5
sele 2
COPY TO d:\月综合开发数据\201412\实际201412\新老区新井产液量表.xls fields ofn,c1,c2,c5,c3,c4,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21 type xl5
sele 1
USE d:\月综合开发数据\201412\实际201412\201412-a
COPY TO d:\月综合开发数据\201412\实际201412\油田注水量构成数据表1.dbf fields ofn,a12,a63,a66,b->c25,b->c23,b->c24,b->c26,b->c27,b->c28,b->c29,b->c30,b->c31,b->c32,b->c33,b->c34,b->c35,b->c36 
  sele 4
   USE d:\月综合开发数据\201412\实际201412\油田注水量构成数据表1.dbf
   go top
   do while not eof()
    sele 2
    loca for ofn=d->ofn
    if found()
     sele 4
     repl c25 with b->c25,c23 with b->c23,c24 with b->c24,c26 with b->c26,c27 with b->c27,c28 with b->c28,c29 with b->c29,c30 with b->c30
     repl c31 with b->c31,c32 with b->c32,c33 with b->c33,c34 with b->c34,c35 with b->c35,c36 with b->c36
     endif
    sele 4
    skip
    enddo
  sele 4
  COPY TO d:\月综合开发数据\201412\实际201412\油田注水量构成数据表1.xls type xl5
  
sele 2
COPY TO d:\月综合开发数据\201412\实际201412\油田注水量构成数据表2.xls fields ofn,c37,c38,c39,c40,c41,c42,c43,c44,c45,c46,c47,c48,c49,c50,c51,c52 type xl5
sele 1
COPY TO d:\月综合开发数据\201412\实际201412\油水井动态表.xls fields ofn,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18 type xl5
sele 1
COPY TO d:\月综合开发数据\201412\实际201412\产液量构成表1.dbf fields ofn,a55,a57,b->c13,b->c14,a75,a76,a82,a83,c->b6,c->b7,c->b13,c->b14
   sele 4
    USE d:\月综合开发数据\201412\实际201412\产液量构成表1.dbf
    go top
     do while not eof()
      sele 2
      loca for ofn=d->ofn
       if found()
        sele 4
        repl c13 with b->c13,c14 with b->c14
       endif
     sele 3
     loca for ofn=d->ofn
       if found()
        sele 4
        repl b6 with c->b6,b7 with c->b7,b13 with c->b13,b14 with c->b14
       endif
    sele 4
    skip
    enddo
  COPY TO d:\月综合开发数据\201412\实际201412\产液量构成表1.xls type xl5
sele 3
COPY TO d:\月综合开发数据\201412\实际201412\产液量构成表2.xls fields  ofn,b20,b21,b27,b28,b34,b35,b41,b42,b48,b49,b55,b56 type xl5
COPY TO d:\月综合开发数据\201412\实际201412\产液量构成表3.dbf fields  ofn,b62,b63,b69,b70,b92,b93,b99,b100,b->c25,b->c53,b->c54,b->c55,b->c56 
 sele 4
  USE d:\月综合开发数据\201412\实际201412\产液量构成表3
  go top
   do while not eof()
     sele 2
     loca for ofn=d->ofn
      if found()
       sele 4
       repl c25 with b->c25,c53 with b->c53,c54 with b->c54,c55 with b->c55,c56 with b->c56
      endif
   sele 4
   skip
  enddo
  COPY TO d:\月综合开发数据\201412\实际201412\产液量构成表3.xls type xl5
USE d:\月综合开发数据\201412\实际201412\kf201412
COPY TO d:\月综合开发数据\201412\实际201412\油田（区块）开发综合数据表1.xls fields ofn,k1,k2,k3,k4,k5,k6,k8,k10,k16,k17,k9,k11,k22,k23,k7,k42,k43,k44,k12,k18,k19,k13,k14,k20,k21 type xl5
COPY TO d:\月综合开发数据\201412\实际201412\油田（区块）开发综合数据表2.xls fields ofn,k24,k25,k26,k52,k53,k54,k27,k28,k55,k15,k29,k30,k50,k41,k31,k32,k33,k34,k35,k36,k37,k38,k39,k40 type xl5
close data
?'实际201412综合开发数据计算完毕'



*******计算油田公司201412综合开发数据*******
?'计算油田公司201412综合开发数据'

use D:\月综合开发数据\201412\油田公司201412\ds201412
copy to D:\月综合开发数据\201412\油田公司201412\油田公司ds201412.xls type xl5
use D:\月综合开发数据\201412\油田公司201412\du201412
copy to D:\月综合开发数据\201412\油田公司201412\油田公司du201412.xls type xl5
use D:\月综合开发数据\201412\油田公司201412\uc201412
copy to D:\月综合开发数据\201412\油田公司201412\油田公司uc201412.xls type xl5
copy to D:\月综合开发数据\201412\油田公司201412\uc.xls type xl5
use D:\月综合开发数据\201412\油田公司201412\sc201412
copy to D:\月综合开发数据\201412\油田公司201412\油田公司sc201412.xls type xl5
copy to D:\月综合开发数据\201412\油田公司201412\sc.xls type xl5

USE d:\月综合开发数据\201412\油田公司201412\Du201412.dbf EXCLUSIVE
COPY TO d:\月综合开发数据\201412\油田公司201412\采油单井.XLS FIELDS Du201412.well,du201412.ofn,Du201412.u1,Du201412.u2,Du201412.u15,Du201412.u7,Du201412.u8,Du201412.u3,Du201412.u4,Du201412.u5,Du201412.u6,Du201412.u9,Du201412.u10,Du201412.u11,Du201412.u12,Du201412.u13,Du201412.u14,Du201412.u17,Du201412.u18,Du201412.u19,Du201412.u20,Du201412.u22,Du201412.u21,Du201412.u24,Du201412.u23,Du201412.u29,Du201412.u25,Du201412.u16,Du201412.BZ TYPE XL5
USE d:\月综合开发数据\201412\油田公司201412\Ds201412.dbf EXCLUSIVE
COPY TO d:\月综合开发数据\201412\油田公司201412\注水单井.XLS FIELDS Ds201412.well,ds201412.ofn,Ds201412.cw,Ds201412.s1,Ds201412.s15,Ds201412.s2,Ds201412.s3,Ds201412.s4,Ds201412.s9,Ds201412.s13,Ds201412.s5,Ds201412.s6,Ds201412.s14,Ds201412.s10,Ds201412.s11,Ds201412.s7,Ds201412.s8,Ds201412.s12,Ds201412.bz TYPE XL5
USE d:\月综合开发数据\201412\油田公司201412\Kf201412.dbf EXCLUSIVE
COPY TO d:\月综合开发数据\201412\油田公司201412\开发数据1.xls fields ofn,k1,k2,k8,k5,k6,k49,k24,k25,k26,k52,k53,k54,k4,k15,k50 type xl5
COPY TO d:\月综合开发数据\201412\油田公司201412\开发数据2.XLS FIELDS ofn,Kf201412.k8,Kf201412.k10,Kf201412.k16,Kf201412.k17,Kf201412.k9,Kf201412.k11,Kf201412.k22,Kf201412.k23,Kf201412.k7,Kf201412.k42,Kf201412.k43,Kf201412.k44 TYPE XL5
COPY TO d:\月综合开发数据\201412\油田公司201412\开发数据3.XLS FIELDS Kf201412.ofn,Kf201412.k13,Kf201412.k14,Kf201412.k20,Kf201412.k21,Kf201412.k56,Kf201412.k57,Kf201412.k58,Kf201412.k59,Kf201412.k45,Kf201412.k46,Kf201412.k60,Kf201412.k47 TYPE XL5
COPY TO d:\月综合开发数据\201412\油田公司201412\开发数据4.XLS FIELDS Kf201412.ofn,Kf201412.k12,Kf201412.k18,Kf201412.k19,Kf201412.k31,Kf201412.k32,Kf201412.k33,Kf201412.k34,Kf201412.k35,Kf201412.k36,Kf201412.k37,Kf201412.k38,Kf201412.k39,Kf201412.k40 TYPE XL5
sele 1
USE d:\月综合开发数据\201412\油田公司201412\201412-a
sele 2
USE d:\月综合开发数据\201412\油田公司201412\201412-c
sele 1
COPY TO d:\月综合开发数据\201412\油田公司201412\产油量构成1.dbf fields ofn,a5,a47,a49,b->c1,b->c2,b->c3,b->c4,a72,a73,a74,a77,a79,a78,a80,a81,a84,a85 
  sele 4
   USE d:\月综合开发数据\201412\油田公司201412\产油量构成1.dbf
    go top
    do while not eof()
     sele 2
      loca for ofn=d->ofn
       if found()
         sele 4
         repl c1 with b->c1,c2 with b->c2,c3 with b->c3,c4 with b->c4
       endif
   sele 4
   skip
   enddo
sele 4
 copy to  d:\月综合开发数据\201412\油田公司201412\产油量构成1.XLS  type XL5
 sele 3  
USE d:\月综合开发数据\201412\油田公司201412\201412-b
COPY TO d:\月综合开发数据\201412\油田公司201412\产油量构成2.xls fields ofn,b1,b3,b4,b5,b8,b10,b11,b12,b15,b17,b18,b19,b22,b24,b25,b26 type xl5
COPY TO d:\月综合开发数据\201412\油田公司201412\产油量构成3.xls fields ofn,b29,b31,b32,b33,b36,b38,b39,b40,b43,b45,b46,b47,b50,b52,b53,b54,b57,b59,b60,b61 type xl5
COPY TO d:\月综合开发数据\201412\油田公司201412\产油量构成4.xls fields ofn,b64,b66,b67,b68,b71,b73,b74,b75,b78,b80,b81,b82,b87,b89,b90,b91,b94,b96,b97,b98 type xl5
sele 2
COPY TO d:\月综合开发数据\201412\油田公司201412\新老区新井产液量表.xls fields ofn,c1,c2,c5,c3,c4,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21 type xl5
sele 1
USE d:\月综合开发数据\201412\油田公司201412\201412-a
COPY TO d:\月综合开发数据\201412\油田公司201412\油田注水量构成数据表1.dbf fields ofn,a12,a63,a66,b->c25,b->c23,b->c24,b->c26,b->c27,b->c28,b->c29,b->c30,b->c31,b->c32,b->c33,b->c34,b->c35,b->c36 
  sele 4
   USE d:\月综合开发数据\201412\油田公司201412\油田注水量构成数据表1.dbf
   go top
   do while not eof()
    sele 2
    loca for ofn=d->ofn
    if found()
     sele 4
     repl c25 with b->c25,c23 with b->c23,c24 with b->c24,c26 with b->c26,c27 with b->c27,c28 with b->c28,c29 with b->c29,c30 with b->c30
     repl c31 with b->c31,c32 with b->c32,c33 with b->c33,c34 with b->c34,c35 with b->c35,c36 with b->c36
     endif
    sele 4
    skip
    enddo
  sele 4
  COPY TO d:\月综合开发数据\201412\油田公司201412\油田注水量构成数据表1.xls type xl5
  
sele 2
COPY TO d:\月综合开发数据\201412\油田公司201412\油田注水量构成数据表2.xls fields ofn,c37,c38,c39,c40,c41,c42,c43,c44,c45,c46,c47,c48,c49,c50,c51,c52 type xl5
sele 1
COPY TO d:\月综合开发数据\201412\油田公司201412\油水井动态表.xls fields ofn,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18 type xl5
sele 1
COPY TO d:\月综合开发数据\201412\油田公司201412\产液量构成表1.dbf fields ofn,a55,a57,b->c13,b->c14,a75,a76,a82,a83,c->b6,c->b7,c->b13,c->b14
   sele 4
    USE d:\月综合开发数据\201412\油田公司201412\产液量构成表1.dbf
    go top
     do while not eof()
      sele 2
      loca for ofn=d->ofn
       if found()
        sele 4
        repl c13 with b->c13,c14 with b->c14
       endif
     sele 3
     loca for ofn=d->ofn
       if found()
        sele 4
        repl b6 with c->b6,b7 with c->b7,b13 with c->b13,b14 with c->b14
       endif
    sele 4
    skip
    enddo
  COPY TO d:\月综合开发数据\201412\油田公司201412\产液量构成表1.xls type xl5
sele 3
COPY TO d:\月综合开发数据\201412\油田公司201412\产液量构成表2.xls fields  ofn,b20,b21,b27,b28,b34,b35,b41,b42,b48,b49,b55,b56 type xl5
COPY TO d:\月综合开发数据\201412\油田公司201412\产液量构成表3.dbf fields  ofn,b62,b63,b69,b70,b92,b93,b99,b100,b->c25,b->c53,b->c54,b->c55,b->c56 
 sele 4
  USE d:\月综合开发数据\201412\油田公司201412\产液量构成表3
  go top
   do while not eof()
     sele 2
     loca for ofn=d->ofn
      if found()
       sele 4
       repl c25 with b->c25,c53 with b->c53,c54 with b->c54,c55 with b->c55,c56 with b->c56
      endif
   sele 4
   skip
  enddo
  COPY TO d:\月综合开发数据\201412\油田公司201412\产液量构成表3.xls type xl5
USE d:\月综合开发数据\201412\油田公司201412\kf201412
COPY TO d:\月综合开发数据\201412\油田公司201412\油田（区块）开发综合数据表1.xls fields ofn,k1,k2,k3,k4,k5,k6,k8,k10,k16,k17,k9,k11,k22,k23,k7,k42,k43,k44,k12,k18,k19,k13,k14,k20,k21 type xl5
COPY TO d:\月综合开发数据\201412\油田公司201412\油田（区块）开发综合数据表2.xls fields ofn,k24,k25,k26,k52,k53,k54,k27,k28,k55,k15,k29,k30,k50,k41,k31,k32,k33,k34,k35,k36,k37,k38,k39,k40 type xl5
close data
?'油田公司201412综合开发数据计算完毕'





