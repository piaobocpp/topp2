&& ********************
&&    油水井井组计算
&& ******************** 
kaishi=datetime()
yyyy=2014  &&修改为当前年份
do while yyyy<=2015
	mm=1 &&修改为当前月份
	do while mm<=12
		do case
			case mm=1
				yymm1=str(yyyy,4,0)+str(0,1,0)+str(mm,1,0)
				yymm2=str(yyyy-1,4,0)+str(12,2,0)
			case mm>1 and mm<10
				yymm1=str(yyyy,4,0)+str(0,1,0)+str(mm,1,0)
				yymm2=str(yyyy,4,0)+str(0,1,0)+str(mm-1,1,0)
			case mm=10
				yymm1=str(yyyy,4,0)+str(mm,2,0)
				yymm2=str(yyyy,4,0)+str(0,1,0)+str(9,1,0)
			case mm>10 and mm<13
				yymm1=str(yyyy,4,0)+str(mm,2,0)
				yymm2=str(yyyy,4,0)+str(mm-1,2,0)
		endcase
		
		?'当前月：'+yymm1+'上月：'+yymm2
		
		file5='D:\分层开发数据\DUDS\DU'+yymm1+'.DBF'
		file6='D:\分层开发数据\DUDS\DS'+yymm1+'.DBF'
		FILE3='D:\分层开发数据\fUfS\fufs\FU'+yymm1+'.DBF'
		FILE4='D:\分层开发数据\fUfS\fufs\FS'+yymm1+'.DBF'

		tt=val(subs(yymm1,1,4))
		t1=val(subs(yymm1,5))

		if t1=1.or.t1=3.or.t1=5.or.t1=7.or.t1=8.or.t1=10.or.t1=12
		   dad=31
		else
		   dad=30
		endif
		if t1=2
		   if mod(tt,4)=0
		      dad=29
		   else   
		      dad=28
		   endif
		endif 

		dime dd(30),num(200)
		sele 2
		use &file5  &&月油
		sele 3
		use &file6  &&月水
		sele 4
		use D:\分层开发数据\OFYCWG\OFYC
		sele 5
		use &file3  &&油井井组
		repl all js with 0
		sele 6
		use &file4
		sele 7
		use D:\分层开发数据\OFYCWG\wg

		sele 4
			do while .not.eof()
		    	i=1
		    	do while i<=30
		    		dd(i)=0
		        	i=i+1
		     	enddo		
				?'正在处理'+ alltrim(ofname)+'油田'+ alltrim(ofjc)+yymm1+'的数据, 请等候! '	

		      	sele 7
		      	loca for ofname=d->ofname.and.ofjc=d->ofjc.and.yymm=yymm1
		      	if eof()
		         	appe blan
		         	repl ofname with d->ofname,ofjc with d->ofjc,yymm with yymm1          
		      	endif
		      	number=recn()
		******************处理油井井组数据******************
		      	sele 5
		      	loca for ofjc=d->ofjc.and.ofname=d->ofname		      

		      	do while .not.eof()
		         	sele 2
		         	loca for trim(well)=trim(e->well).and.u1<>0
		         	if .not.eof()
		        *    repl js with 1
		            	if e->sxs<>0.or.e->uxs<>0
		               		dd(1)=dd(1)+1
		               		dd(26)=dd(26)+u1
		               		dd(6)=dd(6)+u12*e->uxs
		              		dd(9)=dd(9)+u13*e->sxs
		               		dd(24)=dd(24)+u14*e->qxs
		               		if val(u21)<>0
		                  		dd(30)=dd(30)+1
		                  		dd(13)=dd(13)+val(u21)
		               		endif
		               		sele 5
		               		repl js with 1
		               		sele 2
		            	endif
		         	endif
		         	sele 5
		         	cont
		      	enddo
		      	if dd(1)<>0
		         	dd(2)=round(dd(26)*100/(dad*dd(1)),1)
		      	endif
		*******      if d->umd<>0
		*******         dd(5)=round((dd(6)/d->umd+dd(9))/dad,0)
		*******      endif
		      	dd(5)=round((dd(6)+dd(9))/dad,0)
		      	dd(3)=round(dd(6)/dad,0)
		      	dd(4)=round(dd(9)/dad,0)
		      	dd(7)=dd(6)
		      	sele 7
		      	if subs(yymm1,5)<>'01'
		        	loca for ofname=d->ofname.and.ofjc=d->ofjc.and.yymm=yymm2
		         	if .not.eof()
		            	dd(7)=dd(7)+g7
		         	endif
		      	endif
		      	dd(10)=dd(9)
		      	if subs(yymm1,5)<>'01'
		         	loca for ofname=d->ofname.and.ofjc=d->ofjc.and.yymm=yymm2
		         	if .not.eof()
		            	dd(10)=dd(10)+g10
		         	endif
		      	endif
		      	dd(8)=dd(6)/10000
		      	loca for ofname=d->ofname.and.ofjc=d->ofjc.and.yymm=yymm2
		      	if .not.eof()
		         	dd(8)=dd(8)+g8
		      	endif
		      	dd(11)=dd(9)/10000
		      	loca for ofname=d->ofname.and.ofjc=d->ofjc.and.yymm=yymm2
		      	if .not.eof()
		         	dd(11)=dd(11)+g11
		     	endif
		      	dd(25)=dd(24)/10000
		      	loca for ofname=d->ofname.and.ofjc=d->ofjc.and.yymm=yymm2
		      	if .not.eof()
		         	dd(25)=dd(25)+g25
		      	endif
		      	if dd(6)+dd(9)<>0
		         	dd(12)=round(dd(9)*100/(dd(6)+dd(9)),1)
		      	endif
		      	if d->jzcl<>0
		         	dd(14)=round((dd(6)/dad*365)/(d->jzcl*100),2)
		         	dd(15)=round(dd(8)*100/d->jzcl,2)
		      	endif
		      	if dd(30)<>0
		         	dd(13)=round(dd(13)/dd(30),0)
		      	endif
		****************************************************
		******************处理水井井组数据******************
		      	sele 6
		      	loca for ofjc=d->ofjc.and.ofname=d->ofname
		      	do while .not.eof()
		         	sele 3
		         	loca for trim(well)=trim(f->well).and.s1<>0
		         *loca for ofname=d->ofname.and.well=f->well.and.s1<>0
		         	if .not.eof()
		            	if f->zxs<>0
		               		dd(16)=dd(16)+1
		               		dd(27)=dd(27)+s1
		               		IF ZQBZ
		                		DD(19)=DD(19)+S6*0.01280*F->ZXS
		               		ELSE
		               			dd(19)=dd(19)+s6*f->zxs
		               		ENDIF
		            	endif
		         	endif
		         	sele 6
		         	cont
		      	enddo
		      	if dd(16)<>0
		         	dd(17)=round(dd(27)*100/(dad*dd(16)),1)
		      	endif
		      	dd(18)=round(dd(19)/dad,0)
		      	dd(20)=dd(19)
		      	sele 7
		      	if subs(yymm1,5)<>'01'
		         	loca for ofname=d->ofname.and.ofjc=d->ofjc.and.yymm=yymm2
		         	if .not.eof()
		            	dd(20)=dd(20)+g20
		         	endif
		      	endif
		      	dd(21)=dd(19)/10000
		      	loca for ofname=d->ofname.and.ofjc=d->ofjc.and.yymm=yymm2
		      	if .not.eof()
		         	dd(21)=dd(21)+g21
		      	endif
		      	IF trim(OFJC)='长6注气井组'
		      		DD(28)=DD(6)*D->XS+DD(9)+DD(24)*0.00781
		      		DD(29)=DD(8)*D->XS+DD(11)+DD(25)*0.00781
		      	ELSE
		      		dd(28)=dd(6)*d->xs+dd(9)
		      		dd(29)=dd(8)*d->xs+dd(11)
		      	ENDIF
		      	if dd(28)<>0
		         	dd(22)=round(dd(19)/dd(28),2)
		      	endif
		     
		      	if dd(29)<>0
		         	dd(23)=round(dd(21)/dd(29),2)
		      	endif
		****************************************************
		      	sele 7
		      	go number
		      	k=1
		      	do while k<=25
		         	if k<10
		            	field1='g'+str(k,1)
		         	else
		            	field1='g'+str(k,2)
		         	endif
		         	repl &field1 with dd(k)
		         	k=k+1
		      	enddo        
		      	sele 4
		      	skip
		   	enddo
*!*			endif
			i=1
			do while i<=7
		   		sps='sele '+str(i,1)
		   		&sps
		   		use
		   		i=i+1
			enddo
		*!*	retu
		*!*	set escape off
*!*			endif
		mm=mm+1
		if yyyy = 2015 and  mm =7		  &&&修改mm为当前月份+1
			exit
		endif
	
	enddo
	yyyy=yyyy+1
enddo

jieshu=datetime()
yongshi=jieshu-kaishi
?'计算完毕'
?kaishi
?jieshu
?yongshi
messagebox("计算完毕! 用时："+ str(int(yongshi/3600))+"小时" + str(int(mod(yongshi,3600)/60)) +"分" + str(int(mod(yongshi,60))) + "秒")