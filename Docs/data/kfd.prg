* 程序名:       KFD.PRG
*
* -----------------------------------
SET TALK OFF

SET DEVICE TO SCREEN
SET COLOR TO G+/N
CLEAR
@ 0,1 say " ---- 各油区名代码 ------- "
@ 1,1 say "大庆：dq  吉林：jl  辽河：lh    华北：hb    大港：dg " 
@ 2,1 say "冀东：jd  胜利：sl  中原：zy    河南：hn    江汉：jh "
@ 3,1 say "江苏：js  新疆：xj  长庆：cq    玉门：ym    吐哈：th "
@ 4,1 say "青海：qh  四川：sc  塔里木：tm  滇黔桂：qi  安徽：ah "
*
  ACCEPT "输入油区名代码:" TO YQM
  IF ASC(YQM)=0
    CLEAR
   RETURN
  ENDIF
  YQA=UPPER(YQM)
*
  ACCEPT "输入数据文件名:" TO SE
  IF ASC(SE)=0
    CLEAR
   RETURN
  ENDIF
*
FND=SE+"-D"
*
 NA=LEFT(SE,4)
 YA=SUBSTR(SE,5,2)
 I=VAL(YA)
*
DO CASE
   CASE I=1
        YA1=RIGHT(YA,1)
        FDA=YQM+YA1
*        EXIT
* ---------------------------------
   CASE I=2
        YA1=RIGHT(YA,1)
        FDA=YQM+YA1
* ---------------------------------
   CASE I=3
        YA1=RIGHT(YA,1)
        FDA=YQM+YA1
* ---------------------------------
   CASE I=4
        YA1=RIGHT(YA,1)
        FDA=YQM+YA1
* ---------------------------------
   CASE I=5
        YA1=RIGHT(YA,1)
        FDA=YQM+YA1
* ---------------------------------
   CASE I=6
        YA1=RIGHT(YA,1)
        FDA=YQM+YA1
* ---------------------------------
   CASE I=7
        YA1=RIGHT(YA,1)
        FDA=YQM+YA1
* ---------------------------------
   CASE I=8
        YA1=RIGHT(YA,1)
        FDA=YQM+YA1
* ---------------------------------
   CASE I=9
        YA1=RIGHT(YA,1)
        FDA=YQM+YA1
* ---------------------------------
   CASE I=10
        YA1="A"
        FDA=YQM+YA1
* ---------------------------------
   CASE I=11
        YA1="B"
        FDA=YQM+YA1
* ---------------------------------
   CASE I=12
        YA1="C"
        FDA=YQM+YA1
* ---------------------------------
ENDCASE
*
SET COLOR TO G+/N
@ 10,10 SAY "   正在转换 9xxx -> dxxxx 文件    "       
DO D1001
do d1002
do d1003
do d10031
do d1004
do d1005
do d10051
DO D1102
* ------- 判断D文件是否存在 -------------
SET COLOR TO G+/N
IF FILE(FND+".DBF")
   do d10061
   do d10062
   do d10063
ELSE
    @ 1,5 SAY "  没有D文件 "
ENDIF
*
clear
return
